#include "MyNode.h"


bool MyNode::search(MyNode* goal)
{
	for (size_t i = 0; i < size(); ++i)
	{
		// compute distance from current pos to connection
		float x = m_pos[0] - m_connections[i]->m_pos[0];
		float y = m_pos[1] - m_connections[i]->m_pos[1];
		float d = 0.1f*sqrtf(x*x + y*y);

		// add distance already traveled up to this point
		d += m_dist;

		// update distance value if needed
		if (m_connections[i]->m_dist > d)
		{
			m_connections[i]->m_dist = d;
		}
	}
	m_visited = true;
	//if(goal == this)
	//	return false;
	for (size_t i = 0; i < size(); ++i)
	{
		if (!m_connections[i]->visited())
		{
			if (!m_connections[i]->search(goal))
			{
				return false;
			}
		}
	}
	return true;
}

/// \brief	traverses backwards from the goal to the start, always choosing the path of least cost as it goes. 
void MyNode::buildPath(MyNode* start, std::vector<MyNode*>& path)
{
	path.push_back(this);
	if (this != start)
	{
		float d = 99.99f;
		MyNode* n = 0;
		for (size_t i = 0; i < size(); ++i)
		{
			if (d > m_connections[i]->m_dist)
			{
				d = m_connections[i]->m_dist;
				n = m_connections[i];
			}
		}
		if (n)
		{
			n->buildPath(start, path);
		}
	}
}
