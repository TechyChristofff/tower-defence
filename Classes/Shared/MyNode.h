#pragma once
#include <vector>
#include <cfloat>
#include <cmath>
#include "cocos2d.h"

class MyNode
{
public:

	//Brief	initialises the MyNode in the given location
	inline MyNode(float x, float y) : m_connections()
	{
		initSearch();
		m_pos[0] = x;
		m_pos[1] = y;
		//sprite = GameSprite::gameSpriteWithFile(fileName);
		//sprite->setPosition(x, y);
		/*std::strcpy(name , filename);
		name.copy(filename);*/
		hasObject = false;
	}

	//Initialises this MyNode for a search
	inline void initSearch()
	{
		m_dist = 99.99f;
		m_visited = false;
	}

	//Updates the current distance only if the new value improves on the old one
	inline void setDistance(float d)
	{
		if (d < m_dist) m_dist = d;
	}

	//Returns the position of the MyNode
	inline float* pos()
	{
		return m_pos;
	}
	inline const float* pos() const
	{
		return m_pos;
	}

	//Returns the current distance cost
	inline float distance() const
	{
		return m_dist;
	}

	//Returns the visited status (if true, don't recurse over this MyNode again)
	inline bool visited() const
	{
		return m_visited;
	}

	//Returns the number of connections to this MyNode 
	inline size_t size() const
	{
		return m_connections.size();
	}

	//Returns a connected MyNode
	inline MyNode* link(size_t i)
	{
		return m_connections[i];
	}

	//Returns a connected MyNode
	inline const MyNode* link(size_t i) const
	{
		return m_connections[i];
	}

	//Searches the graph recursively until the goal is found.
	bool search(MyNode* goal);

	//Traverses backwards from the goal to the start, always choosing the path of least cost as it goes. 
	void buildPath(MyNode* start, std::vector<MyNode*>& path);

	friend void connect(MyNode* a, MyNode* b);

private:
	float m_pos[2];
	float m_dist;
	bool m_visited;
	bool hasObject;
	std::vector<MyNode*> m_connections;
	std::string name;
};

inline void connect(MyNode* a, MyNode* b)
{
	a->m_connections.push_back(b);
	b->m_connections.push_back(a);
}
