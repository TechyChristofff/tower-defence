#include "GameScene.h"

USING_NS_CC;

using namespace cocos2d;

//GameScene::GameScene()
//{
//}
//
//GameScene::~GameScene()
//{
//}

Scene* GameScene::createScene()
{
	// 'scene' is an autorelease object
	auto scene = Scene::create();

	// 'layer' is an autorelease object
	auto layer = GameScene::create();

	// add layer as a child to scene
	scene->addChild(layer);

	// return the scene
	return scene;
}


 void GameScene::update(float dt)
{

}

 bool GameScene::init()
 {

	 ////////////////////////////////////////////////////////////////////////////////Init Stuff
	 if (!PanZoomLayer::init())
	 {
		 return false;
	 }
	 
	 Size visibleSize = Director::getInstance()->getVisibleSize();
	 Vec2 origin = Director::getInstance()->getVisibleOrigin();

	 auto closeItem = MenuItemImage::create(
		 "CloseNormal.png",
		 "CloseSelected.png",
		 CC_CALLBACK_1(GameScene::menuCloseCallback, this));

	 closeItem->setPosition(Vec2(origin.x + visibleSize.width - closeItem->getContentSize().width / 2,
		 origin.y + closeItem->getContentSize().height / 2));

	 // create menu, it's an autorelease object
	 auto menu = Menu::create(closeItem, NULL);
	 menu->setPosition(Vec2::ZERO);
	 this->addChild(menu, 100);

	 tileMap = TMXTiledMap::create("TestMap.tmx");
	 background = tileMap->layerNamed("Map");

	 tileMap->create("TestMap.tmx");


	 addChild(tileMap, 0);

	 // all tiles are aliased by default, let's set them anti-aliased
	 for (const auto& child : tileMap->getChildren())
	 {
		 static_cast<SpriteBatchNode*>(child)->getTexture()->setAntiAliasTexParameters();
	 }

	 this->setTouchEnabled(true);
	
	 TMXObjectGroup *objectGroup = tileMap->objectGroupNamed("Objects");

	 if (objectGroup == NULL){
		 CCLog("tile map has no objects object layer");
		 return false;
	 }
 
	 //meta = tileMap->getLayer("Map");
	 //meta = tileMap->layerNamed("Map");

	 CCLOG("GetLayer = %f , LayerNamed = %f ", tileMap->getLayer("Map"), tileMap->layerNamed("Map"));

	// meta->setVisible(false);

	 float x = 50;
	 float y = 50;

	 player = Sprite::create("RedBox.png");
	 player->setPosition(x,y);

	 this->addChild(player);
	 this->setViewPointCenter(player->getPosition());

	 auto listener = EventListenerTouchOneByOne::create();
	 listener->setSwallowTouches(true);

	 listener->onTouchBegan = CC_CALLBACK_2(GameScene::onTouchBegan, this);
	 listener->onTouchMoved = CC_CALLBACK_2(GameScene::onTouchMoved, this);
	 listener->onTouchEnded = CC_CALLBACK_2(GameScene::onTouchEnded, this);

	 _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);

	 //this->objectsInScene = cocos2d::Vector < PlayerObj* > {100};

	 return true;
 }

 void GameScene::setViewPointCenter(CCPoint position)
 {

	 Size winSize = Director::sharedDirector()->getWinSize();

	 int x = MAX(position.x, winSize.width / 2);
	 int y = MAX(position.y, winSize.height / 2);
	 x = MIN(x, (tileMap->getMapSize().width * this->tileMap->getTileSize().width) - winSize.width / 2);
	 y = MIN(y, (tileMap->getMapSize().height * tileMap->getTileSize().height) - winSize.height / 2);
	 Point actualPosition = ccp(x, y);

	 Point centerOfView = ccp(winSize.width / 2, winSize.height / 2);
	 Point viewPoint = ccpSub(centerOfView, actualPosition);
	 this->setPosition(viewPoint);
 }

 bool GameScene::onTouchBegan(Touch *touch, CCEvent *event)
 {
	 CCLOG("ontouchBegin x = %f, y = %f", touch->getLocation().x, touch->getLocation().y);

	/* PlayerObj* newObject = new PlayerObj("YellowBox.png");
	 newObject->setPosition(ccp(touch->getLocation().x, touch->getLocation().y));*/
	 //this->objectsInScene.pushBack(newObject);
	 //this->addChild(newObject);

	 Point touchLocation = touch->getLocationInView();
	 touchLocation = Director::sharedDirector()->convertToGL(touchLocation);
	 touchLocation = this->convertToNodeSpace(touchLocation);

	 std::string tag = "lookatme";

	 newObject = new PlayerObj("YellowBox.png", tag);
	 newObject->setPosition(touchLocation.x, touchLocation.y);
	 this->addChild(newObject->returnSprite());
	 

	/* Sprite *testing = Sprite::create("YellowBox.png");
	 this->addChild(testing);
	 testing->setPosition(touchLocation.x,touchLocation.y);*/
	 
	 return true;
 }

 Point GameScene::tileCoordForPosition(Point position)
 {
	 int x = position.x / tileMap->getTileSize().width;
	 int y = ((tileMap->getMapSize().height * tileMap->getTileSize().height) - position.y) /tileMap->getTileSize().height;
	 return ccp(x, y);
 }

 void GameScene::onTouchMoved(Touch *touch, Event *event)
 {
	 CCLOG("ontouchMoved x = %f, y = %f", touch->getLocation().x, touch->getLocation().y);

	 Point touchLocation = touch->getLocationInView();
	 touchLocation = Director::sharedDirector()->convertToGL(touchLocation);
	 touchLocation = this->convertToNodeSpace(touchLocation);

	 newObject->setPosition(touchLocation.x, touchLocation.y);

	/* Point touchLocation = touch->getLocationInView();
	 Point trueTouchPoint = this->tileCoordForPosition(touchLocation);

	 this->setViewPointCenter(trueTouchPoint);*/
	 
 }

 void GameScene::onTouchEnded(Touch *touch, Event *event)
 {
	 //Point touchLocation = touch->getLocationInView();
	 //touchLocation = Director::sharedDirector()->convertToGL(touchLocation);
	 //touchLocation = this->convertToNodeSpace(touchLocation);

	 //Point playerPos = player->getPosition();
	 //Point diff = ccpSub(touchLocation, playerPos);


	 //float time = touchLocation.distance(playerPos) / 3;

	 //if (abs(diff.x) > abs(diff.y)) {
		// if (diff.x > 0) {
		//	 playerPos.x += tileMap->getTileSize().width;
		// }
		// else {
		//	 playerPos.x -= tileMap->getTileSize().width;
		// }
	 //}
	 //else {
		// if (diff.y > 0) {
		//	 playerPos.y += tileMap->getTileSize().height;
		// }
		// else {
		//	 playerPos.y -= tileMap->getTileSize().height;
		// }
	 //}

	 //// safety check on the bounds of the map
	 //if (playerPos.x <= (tileMap->getMapSize().width * tileMap->getTileSize().width) &&
		// playerPos.y <= (tileMap->getMapSize().height * tileMap->getTileSize().height) &&
		// playerPos.y >= 0 &&
		// playerPos.x >= 0)
	 //{
		// this->setPlayerPosition(playerPos);
	 //}

	 //this->setViewPointCenter(player->getPosition());

 }

 void GameScene::setPlayerPosition(CCPoint position) 
 {
	 Point trueTouchPoint = this->tileCoordForPosition(position);
	 //int Gid = meta->getTileGIDAt(trueTouchPoint);

	 player->setPosition(position);
 }

 void GameScene::menuCloseCallback(Ref* pSender)
 {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WP8) || (CC_TARGET_PLATFORM == CC_PLATFORM_WINRT)
	 MessageBox("You pressed the close button. Windows Store Apps do not implement a close button.", "Alert");
	 return;
#endif

	 Director::getInstance()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	 exit(0);
#endif
 }


