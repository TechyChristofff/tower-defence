#pragma once
#include "cocos2d.h"

using namespace cocos2d;

class PlayerObj
{
public:
	PlayerObj(std::string fileName, std::string objectName);
	~PlayerObj();

	void takeDamage(float dmg);

	bool getLoaded();

	Sprite* returnSprite();

	void setPosition(float x, float y);

private:
	float hp;
	std::string tag;
	Sprite *sprite;

	bool loaded;

};

