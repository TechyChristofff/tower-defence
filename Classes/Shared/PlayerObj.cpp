#include "PlayerObj.h"


PlayerObj::PlayerObj(std::string fileName,std::string objectName)
{
	hp = 100;
	tag = objectName;
	sprite = Sprite::create(fileName);
	loaded = true;
}


PlayerObj::~PlayerObj()
{
}

void PlayerObj::takeDamage(float dmg)
{

}

bool PlayerObj::getLoaded()
{
	return loaded;
}

Sprite* PlayerObj::returnSprite()
{
	return sprite;
}

void PlayerObj::setPosition(float x, float y)
{
	if (sprite != 0)
	{
		sprite->setPosition(x,y);
	}
}