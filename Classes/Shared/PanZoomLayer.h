

#ifndef _PAN_ZOOM_LAYER_H_
#define _PAN_ZOOM_LAYER_H_

#include "cocos2d.h"

using namespace cocos2d;

class PanZoomLayer : public LayerColor
{
private:
	Array *_touches;
	Point _beganTouchPoint;
	Point _endedTouchPoint;
	Point _deltaSum;
	Point _prevDeltaPoint;
	double _accelerationFactor;
	Rect _panBoundsRect;
	float _maxScale;
	float _minScale;
	float _productFactor;

	bool _isHolding;


public:
	PanZoomLayer();
	virtual ~PanZoomLayer();

	static PanZoomLayer* create();

	virtual bool init();
	virtual void onEnter();
	virtual void onExit();

	virtual void update( float dt );

	virtual void TouchesBegan(Set *pTouches, Event *pEvent);
	virtual void TouchesMoved(Set *pTouches, Event *pEvent);
	virtual void TouchesEnded(Set *pTouches, Event *pEvent);

	virtual void setPosition( Point position );
	virtual void setScale( float scale );

	void SetPanBoundsRect( Rect rect );
	void SetMaxScale( float maxScale );
	float GetMaxScale();
	void SetMinScale( float minScale );
	float GetMinScale();

	void Holding();
	void UnHolding();

	void SetProductFactor( float v );

};


#endif