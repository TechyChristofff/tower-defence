#pragma once

#include "cocos2d.h"
#include "Shared\MyNode.h"
#include <stdlib.h>
#include <stdio.h>
#include <list>
#include <map>
#include <vector>
#include <random>
#include "Shared\PanZoomLayer.h"
#include "Shared\GameObj.h"
#include "Shared\PlayerObj.h"

using namespace cocos2d;


class GameScene :
	public PanZoomLayer
{
public:

	CREATE_FUNC(GameScene);

	static cocos2d::Scene* createScene();
	//static cocos2d::Scene* createScene() { return create(); }

	// Method 'init' in cocos2d-x returns bool, instead of 'id' in cocos2d-iphone (an object pointer)
	virtual bool init();

	virtual std::string screenName()
	{
		return "Game Scene";
	}

	void draw(cocos2d::Renderer *renderer, const Mat4& transform, bool transformUpdated);

	void menuCloseCallback(cocos2d::Ref* pSender);

	void update(float dt);

	void setViewPointCenter(Point position);

	void setPlayerPosition(Point position);

	Point tileCoordForPosition(Point position);

	bool onTouchBegan(Touch *touch, Event *event);

	void onTouchMoved(Touch *touch, Event *event);

	void onTouchEnded(Touch *touch, Event *event);

	//GameScene();
	//~GameScene();

private:
	MyNode* Grid[30][30];
	TMXTiledMap *tileMap;
	TMXLayer *background;
	TMXLayer *meta;
	Sprite *player;
	CCTMXTiledMap *_tileMap;
	CCTMXLayer *_background;

	PlayerObj *test;
	PlayerObj* newObject;

	//Vector<PlayerObj*> objectsInScene;

};

